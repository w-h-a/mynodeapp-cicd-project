#!/usr/bin/env bash

export IMAGE_NAME=$1
docker-compose down
docker-compose up --detach
echo "success"